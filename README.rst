GAVO DaCHS
==========

`GAVO DaCHS`_ (the Data Centre Helper Suite from the German
Astrophyiscal Virtual Observatory) is an integrated package for
publishing astronomical data to the `Virtual Observatory`_.

.. _Virtual Observatory: https://ivoa.net
.. _GAVO DaCHS: https://soft.g-vo.org/dachs

Documentation is maintained `on docs.g-vo.org`_ or, if you don't mind
the occasional ad or some snooping, `at readthedocs`_.  In case you would
rather read some somewhat more scholarly bit on it, there's a (still
largely pertinent) 2014 paper on it: `2014A&C.....7...27D`_ (Elsevier
disclaimer: That was not *really* my decision).

.. _2014A&C.....7...27D: http://adsabs.harvard.edu/abs/2014A&C.....7...27D
.. _at readthedocs: http://dachs-doc.readthedocs.io/
.. _on docs.g-vo.org: https://docs.g-vo.org/DaCHS/

You are absolutely welcome to report bugs or even better contribute code
here; in case you want to do anything requiring credentials and you
cannot figure out how to get any, please contact us at
gavo@ari.uni-heidelberg.de.
