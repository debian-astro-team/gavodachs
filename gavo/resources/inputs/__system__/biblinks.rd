<!-- a resource descriptor for supporting biblinks as per
https://www.ivoa.net/documents/BibVO
-->

<!-- TODO: document! -->

<resource resdir="__system" schema="dc">
	<meta name="creationDate">2023-10-26T20:00:00</meta>
	<meta name="title">\getConfig{web}{sitename} Bibliography Links</meta>
	<meta name="description">These resources let bibliography services
		retrieve triples relating published articles and published
		datasets or data collections.</meta>
	<meta name="subject">virtual-observatories</meta>

	<STREAM id="output-columns">
		<column name="bib_ref" type="text"
			description="The identifier of the bibliographic record
				the link originates at."/>
		<column name="relationship" type="text"
			description="The relationship between bib_ref and dataset_ref.
				This is typically Cites for datasets and isSupplementedBy
				for data collections."/>
		<column name="dataset_ref" type="text"
			description="A reference to the dataset (or data collections),
				very typically as an http URI."/>
		<column name="bib_format" type="text"
			description="The format of bib_ref (e.g., 'doi').
				Letting this be NULL is equivalent to 'bibcode'."/>
		<column name="cardinality" type="integer"
			description="Number of links for indirect (i.e., through a
				per-article landing page) biblinks.">
			<values nullLiteral="0"/>
		</column>
	</STREAM>

	<table id="biblinks" onDisk="True" system="True">
		<meta name="title">Systemwide Bibliography Links</meta>
		<meta name="description">This table contains links between
			bibliographic items and local datasets or data collections.
			It follows https://www.ivoa.net/documents/BibVO and is
			intended to be harvested by bibliography services.
		</meta>
	
		<FEED source="output-columns"/>
		<column name="sourcerd" type="text"
			description="The identifier of the RD that entered this triple"/>
		<column name="link_source" type="text"
			description="An optional identifier for a parsing source from which
				the link was produced; this is only relevant for dropping in
				the presence of incremental parsing."/>
	</table>

	<table id="core-output">
		<FEED source="output-columns"/>
	</table>

	<data id="create_table">
		<make table="biblinks"/>
	</data>

	<service id="links" allowed="biblinks.json">
		<meta name="shortName">\metaString{authority.shortName} BL</meta>
		<biblinksCore/>
		<publish render="biblinks.json" sets="ivo_managed"/>
	</service>
</resource>
