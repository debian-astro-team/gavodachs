<resource schema="tap_user" resdir="__system"
	allProfiles="feed,trustedquery,untrustedquery">
	<meta name="creationDate">2024-09-17T12:00:00</meta>
	<meta name="description">A schema containing users' uploads.  Tables
		uploaded here are persistent across TAP requests but will automatically
		be garbage collected after \getConfig{ivoa}{userTableDays} days.
		Users are most welcome to delete them manually before that time by
		doing a DELETE request against the table URI.</meta>
	<meta name="subject">virtual-observatories</meta>
	<meta name="subject">catalogs</meta>
	<meta name="schema-rank">10</meta>

	<!-- this is only loaded when user tables are actually accessed; hence,
		the following job should only run when there's actually user tables.
		Still, don't assume anything is functional here, as operators will
		receive mails if things go wrong in an exec. -->

	<execute every="-80000" title="Clean user-uploaded tables" id="expire">
		<job>
			<setup imports="gavo.web.tap_uploads"/>
			<code>
				with base.getWritableAdminConn() as conn:
					toDelete = conn.query("select user_name, table_name"
						" from tap_user.tables"
						" where expiry&lt;=CURRENT_TIMESTAMP")
					for userName, tableName in toDelete:
						tap_uploads.dropUserUploadedTable(
							conn, userName, tableName)
			</code>
		</job>
	</execute>

	<table id="tables" onDisk="True" system="True"
		primary="user_name,table_name"
		allProfiles="feed,trustedquery,untrustedquery"
		dupePolicy="overwrite">
		<meta name="description">This table contains metadata for the tables
			uploaded by users.
		</meta>

		<column name="user_name" type="text"
			description="Identifier for the uploading user; NULL is for
				anonymous uploads."/>
		<column name="table_name" type="text"
			description="User-configured table name"/>
		<column name="physical_name" type="text"
			description="Name of the table within the tap_user_tables schema."/>
		<column name="metadata" type="text"
			description="DaCHS tableDef for this table"/>
		<column name="expiry" type="timestamp"
			description="Date and time this table will be garbage collected."/>
	</table>

	<data id="create">
		<make table="tables"/>
	</data>
</resource>

