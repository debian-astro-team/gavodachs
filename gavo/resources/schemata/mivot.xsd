<!-- XML Schema for the VODML lite mapping L. Michel 06/2020 -->
<!-- MIVOT schema for the record -->
<!-- GL 2021-07-23: Refactoring towards using type definitions rather than
    global elements. Following VOTable v 1.11 refactoring from 23-May-2006 LM
    2021-08-25: add VOtable import, prefix prefixes with dm-mapping -->

<!-- ======================= -->
<!--  XSD sample http://users.polytech.unice.fr/~pfz/LANGDOC/COURS/EXEMPLES/XSD_DXS/chapter14.html -->

<xs:schema attributeFormDefault="unqualified"
    elementFormDefault="qualified" version="1.1"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    targetNamespace="http://www.ivoa.net/xml/mivot"
    xmlns:dm-mapping="http://www.ivoa.net/xml/mivot">


    <!-- Required to validate mapping block within a VOTable (LM 08/2021) -->
    <xs:import namespace="http://www.ivoa.net/xml/VOTable/v1.3"
        schemaLocation="http://www.ivoa.net/xml/VOTable/v1.3" />
    <xs:import namespace="http://www.ivoa.net/xml/VOTable/v1.2"
        schemaLocation="http://www.ivoa.net/xml/VOTable/v1.2" />
    <xs:import namespace="http://www.ivoa.net/xml/VOTable/v1.1"
        schemaLocation="http://www.ivoa.net/xml/VOTable/v1.1" />

    <!-- Top level structure of the mapping block -->
    <xs:element name="VODML">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="REPORT" type="dm-mapping:Report"
                    minOccurs="0" maxOccurs="1" />
                <xs:element name="MODEL" type="dm-mapping:Model"
                    minOccurs="0" maxOccurs="unbounded" />
                <xs:element name="GLOBALS" type="dm-mapping:Globals"
                    minOccurs="0" maxOccurs="1" />
                <xs:element name="TEMPLATES" type="dm-mapping:Templates"
                    minOccurs="0" maxOccurs="unbounded" />
            </xs:sequence>
         <!--  MD proposal
         -->
        </xs:complexType>

        <!-- Make sure dmid is unique within the mapping block -->
        <xs:unique name="Uniquedmid">
            <xs:selector xpath=".//*" />
            <xs:field xpath="@dmid" />
        </xs:unique>
    </xs:element>

    <!-- Annotation process report-->
    <xs:complexType name="Report" mixed="true">
        <xs:attribute name="status" type="xs:string"
                      use="required" />

    </xs:complexType>

    <!-- Declaration of one used model -->
    <xs:complexType name="Model">
        <xs:attribute name="name" type="xs:string" />
        <xs:attribute name="url" type="xs:anyURI" />


    </xs:complexType>

    <!-- Mapping of the data that have a global scope (e.g. frames) -->
    <xs:complexType name="Globals">
      <xs:sequence>
        <xs:choice>
            <xs:element name="INSTANCE" type="dm-mapping:Instance"/>
            <xs:element name="COLLECTION" type="dm-mapping:Collection"/>
        </xs:choice>
      </xs:sequence>
    </xs:complexType>

    <!-- Mapping of the data contained in a particular table -->
    <xs:complexType name="Templates">
        <xs:sequence>
            <xs:element name="WHERE" type="dm-mapping:Where"
                minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="INSTANCE" type="dm-mapping:Instance"
                minOccurs="1" maxOccurs="unbounded" />
        </xs:sequence>
        <xs:attribute type="xs:string" name="tableref" />
    </xs:complexType>

    <!-- Mapping of either a Datatype or an Objecttype -->
    <xs:complexType name="Instance">
        <xs:sequence>
            <xs:choice maxOccurs="unbounded">
                <xs:element name="PRIMARY_KEY"
                    type="dm-mapping:PrimaryKey" minOccurs="0" />
            </xs:choice> <!-- can this now be an xs:all -->
            <xs:choice maxOccurs="unbounded">
                <xs:element name="REFERENCE" type="dm-mapping:Reference"
                    minOccurs="0" />
                <xs:element name="ATTRIBUTE" type="dm-mapping:Attribute"
                    minOccurs="0" />
                <xs:element name="INSTANCE" type="dm-mapping:Instance"
                    minOccurs="0" />
                <xs:element name="COLLECTION"
                    type="dm-mapping:Collection" minOccurs="0" />
            </xs:choice>
        </xs:sequence>
        <xs:attribute type="xs:string" name="dmrole"
            use="optional" />
        <xs:attribute type="xs:string" name="dmtype"
            use="required" />
        <xs:attribute type="xs:string" name="dmid" />
    </xs:complexType>

    <!-- Atomic attribute -->
    <xs:complexType name="Attribute">
        <xs:attribute type="xs:string" name="dmrole"
            use="optional" />
        <xs:attribute type="xs:string" name="dmtype"
            use="required" />
        <xs:attribute type="xs:string" name="ref" />
        <xs:attribute type="xs:string" name="value" />
        <xs:attribute type="xs:string" name="unit" />
        <xs:attribute type="xs:string" name="arrayindex" />


    </xs:complexType>

    <!-- Data list mapping block -->
    <xs:complexType name="Collection">
        <xs:choice maxOccurs="unbounded">
            <xs:element name="REFERENCE" type="dm-mapping:Reference"
                minOccurs="0" />
            <xs:element name="INSTANCE" type="dm-mapping:Instance"
                minOccurs="0" />
            <xs:element name="ATTRIBUTE" type="dm-mapping:Attribute"
                minOccurs="0" />
            <xs:element name="COLLECTION" type="dm-mapping:Collection"
                minOccurs="0" />
            <xs:element name="JOIN" type="dm-mapping:Join"
                minOccurs="0" maxOccurs="1" />
        </xs:choice>
        <xs:attribute type="xs:string" name="dmrole"
            use="optional" />
        <xs:attribute type="xs:string" name="size" />
        <xs:attribute type="xs:string" name="dmid" />


    </xs:complexType>

    <xs:complexType name="Reference">
        <xs:sequence>
            <xs:element name="FOREIGN_KEY"
                type="dm-mapping:ForeignKey" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
        <xs:attribute type="xs:string" name="dmrole"
            use="optional" />
        <xs:attribute type="xs:string" name="sourceref" />
        <xs:attribute type="xs:string" name="dmref" />
    </xs:complexType>

    <!-- Join with another table. -->
    <xs:complexType name="Join">
        <xs:sequence>
            <xs:element name="WHERE" type="dm-mapping:Where"
                minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
        <xs:attribute type="xs:string" name="sourceref" />
        <xs:attribute type="xs:string" name="dmref" />
    </xs:complexType>

    <!-- Select table rows with value of the column @ref = @value -->
    <xs:complexType name="Where">
        <xs:attribute type="xs:string" name="foreignkey" />
        <xs:attribute type="xs:string" name="primarykey" />
        <xs:attribute type="xs:string" name="value" />
    </xs:complexType>

    <xs:complexType name="PrimaryKey">
        <xs:attribute type="xs:string" name="ref" />
        <xs:attribute type="xs:string" name="dmtype" />
        <xs:attribute type="xs:string" name="value" />
    </xs:complexType>

    <xs:complexType name="ForeignKey">
        <xs:attribute type="xs:string" name="ref" use="required" />

    </xs:complexType>

</xs:schema>
