<!-- A template for a simple, TAP-accessible table with no further frills.
If you have "typed" data (object catalgues, images, spectra...), use
a more refined template.

To fill it out, search and replace %.*%

Note that this doesn't expose all features of DaCHS.  For advanced
projects, you'll still have to read documentation... -->
\tpldesc{Any sort of data via a plain TAP table}

<resource schema="\resdir" resdir=".">
  <meta name="creationDate">\now</meta>

  \commonmeta
  <table id="main" onDisk="True" adql="True">
    <column name="%enter a name%" type="%sql type: integer, double precisions...%"
      unit="%remove the attribute if unitless%" ucd="%cf. dachs adm suggest%"
      tablehead="%short label for a table header%"
      description="%long, human readable information.  Be precise here%"/>
    %add further columns%
  </table>

  <data id="import">
    <sources pattern="%resdir-relative pattern, like data/*.txt%"/>

    <!-- the grammar really depends on your input material.  See
      http://docs.g-vo.org/DaCHS/ref.html#grammars-available,
      in particular columnGrammar, csvGrammar, fitsTableGrammar,
      and reGrammar; if nothing else helps see embeddedGrammar
      or customGrammar -->
    <csvGrammar names="name1 some_other_name and_so_on"/>

    <make table="main">
      <rowmaker idmaps="*">
        <!-- the following is an example of a mapping rule that uses
        a python expression; @something takes the value of the something
        field returned by the grammar.  You obviously need to edit
        or remove this concrete rule. -->
        <map dest="%name of a column%">int(@some_other_name[2:])</map>
      </rowmaker>
    </make>
  </data>

  <service id="q" allowed="form">
    <!-- if you want a browser-based service in addition to TAP, use
    this.  Otherwise, delete this and just write <publish/> into
    the table element above to publish the table as such.  With a
    service, the table will be published as part of the service -->
    <meta name="shortName">%max. 16 characters%</meta>

    <!-- the browser interface goes to the VO and the front page -->
    <publish render="form" sets="ivo_managed, local"/>
    <!-- all publish elements only become active after you run
      dachs pub q -->

    <dbCore queriedTable="main">
      <!-- to add query constraints on table columns, add condDesc
      elements built from the column -->
      <condDesc buildFrom="%colname%"/>
    </dbCore>
  </service>

  <regSuite title="\resdir regression">
    <regTest title="\resdir table serves some data">
      <url parSet="TAP"
        QUERY="SELECT * FROM \resdir.main WHERE %select one column%"
        >/tap/sync</url>
      <code>
        # The actual assertions are pyUnit-like.  Obviously, you want to
        # remove the print statement once you've worked out what to test
        # against.
        row = self.getFirstVOTableRow()
        print(row)
        self.assertAlmostEqual(row["ra"], 22.22222)
      </code>
    </regTest>

    <!-- add more tests: extra tests for the web side, custom widgets,
      rendered outputFields... -->
  </regSuite>
</resource>
