"""
Instantiated resources (tables, etc), plus data mangling.
"""

#c Copyright 2008-2024, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


# Not checked by pyflakes: API file with gratuitous imports

from gavo.utils import DBTableError
from gavo.rsc.common import FLUSH
from gavo.rsc.dbtable import DBTable
from gavo.rsc.dumping import createDump, restoreDump
from gavo.rsc.qtable import QueryTable
from gavo.rsc.table import BaseTable, InMemoryTable
from gavo.rsc.tables import TableForDef
from gavo.rsc.data import (Data, makeData, wrapTable, makeDependentsFor,
	makeCombinedData, MultiForcedSources)
from gavo.rsc.common import (getParseOptions,
	parseValidating, parseNonValidating)
from gavo.rsc.metatable import MetaTableHandler
