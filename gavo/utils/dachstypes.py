"""
Basic types we will more widely need in DaCHS;  we also import everything
we need from typing, so for our type annotations we only need to say
from utils.dachstypes import ...

This also lets us smuggle in fallbacks for python 3.7 (and we won't go
further back than that).
"""

#c Copyright 2008-2024, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


from typing import ( #noflake: gratuitous imports for re-exporting
	AbstractSet, Any,
	BinaryIO,
	Callable, ContextManager,
	Dict,
	FrozenSet,
	Generator,
	Hashable,
	IO, Iterable, Iterator,
	Generic,
	Hashable,
	List,
	Mapping,
	NewType,
	Optional,
	Sequence, Set,
	TextIO, Tuple, TYPE_CHECKING, Type, TypeVar,
	Union,
	cast)

from types import ModuleType  #noflake gratuitous imports for re-exporting

import pathlib

try:
	from numpy.typing import NDArray
except ImportError:
	NDArray = NewType('NDArray', object)  # type: ignore

Filename = Union[str, pathlib.Path]
# positions when doing XML parsing
ParsePosition =  NewType('ParsePosition', object)
StrOrBytes = Union[str, bytes]
StrToStrMap = Dict[str, str]

# see utils.plainxml
PlainXMLEvent = Tuple[
	str,
	Optional[str],
	Union[Dict[str, str], None, str]]
