"""
TAP 1.2 prototype persistent TAP uploads.

These are implemented as renderers that might work on stuff other than
TAP, put I'm not sure there is a case to be made for persistent *table*
uploads on anything else.  And anyway: these tables have extra features
to be TAP accessible, and they are managed through the
TAP_SCHEMA.persistent_uploads table.
"""

#c Copyright 2008-2024, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.

import datetime
import re

from twisted.internet import threads
from twisted.web import resource
from twisted.web import server

from gavo import base
from gavo import formats
from gavo import rsc
from gavo import rscdef
from gavo import svcs
from gavo import utils
from gavo.base import valuemappers
from gavo.formats import votableread
from gavo.protocols import dali
from gavo.registry import tableset
from gavo.web import vodal
from gavo.web.vosi import VTM


USER_TABLE_NAME_PATTERN = re.compile(r"[a-z][a-z0-9_]*$")


def dropUserUploadedTable(conn, user, tableName):
	"""drops a user-uploaded table.

	This will drop both the entry in the management table and the table
	itself.  It is not an error to request dropping a non-existing table;
	that's a no-op, though.
	"""
	constraints = {"user": user, "tablename": tableName}
	res = list(conn.query("select physical_name from tap_user.tables"
			" WHERE user_name=%(user)s and table_name=%(tablename)s",
			constraints))
	if res:
		physicalName = res[0][0]
		conn.execute(f"DROP TABLE IF EXISTS {physicalName}")

		conn.execute("DELETE FROM tap_user.tables"
				" WHERE user_name=%(user)s and table_name=%(tablename)s",
				constraints)


def uploadUserTable(conn, tableName, srcFile, user):
	"""actually performs a table ingestion.

	It arranges for the data in the VOTable readable from srcFile
	will be tap_user.tableName for user, while it is actually
	stored in tap_user.<something else>.  That something else,
	the physcial_name, is returned by this function.

	The function also performs the basic bookkeeping, including storing a
	VODataService tableset in tap_user.tables.
	"""
	if valuemappers.needsQuoting(tableName):
		raise base.ReportableError(f"'{tableName}' cannot be used as"
			" an upload table name (which must be regular ADQL identifiers,"
			" in particular not ADQL reserved words).")

	# this is where the physical name is defined.  Everyone else
	# must get it from the physcial_name column, as this rule
	# will probably change.
	physicalName = f"tap_user._{user}_{tableName}"
	if not re.match(r"tap_user\.[a-z_0-9]+$", physicalName):
		raise base.ReportableError("Bad generated table name.  Giving up.")
	if base.UnmanagedQuerier(conn).getTableType(physicalName):
		conn.execute(f"DROP table IF EXISTS {physicalName}")

	if srcFile:
		uploadedTable = votableread.uploadVOTable(
			physicalName.split(".")[-1],
			srcFile,
			conn,
			nameMaker=votableread.AutoQuotedNameMaker(),
			rd=base.caches.getRD("//tap_user"),
			temporary=False,
			adql=True)

		# we need to manually give the table an id because structToXML
		# swallows these.  I suppose there should be a way to presere
		# id-s in that function, but knowing when to drop an id and
		# when to preserve it is not simple.
		serialised = base.structToXML(uploadedTable.tableDef).replace(
			"<table>", f'<table id="{tableName}">')
	
	else:
		# without a source file, this is just an allocation (for create table),
		# and we have no idea about the table structure.  Do a placeholder:
		serialised = f'<table id="{tableName}">'

	tablesTable = rsc.TableForDef(
		base.resolveCrossId("//tap_user#tables"),
		connection=conn)
	tablesTable.addRow({
		"user_name": user,
		"table_name": tableName,
		"physical_name": physicalName,
		"metadata": serialised,
		"expiry": datetime.datetime.utcnow()
			+datetime.timedelta(days=base.getConfig("ivoa", "userTableDays"))})
	
	return physicalName


class ResourceWithDALIErrors(resource.Resource):
	"""A class that tries to produce DALI errors on almost all that
	can go wrong in the sync case.

	To work this, don't override render and getChild but render_METHOD
	and _getChildReal.  Only sync errors will be handled, when you defer,
	you need to call addErrback(self._renderError, request) yourself.
	_renderError finishes the request itself.
	"""
	def render(self, request):
		try:
			return resource.Resource.render(self, request)
		except Exception as exc:
			base.ui.notifyError(str(exc))
			return dali.serveDALIError(request, exc)

	def _renderError(self, flr, request):
		base.ui.notifyFailure(flr)
		dali.serveDALIError(request, flr.value)
		return server.NOT_DONE_YET

	def serveImmediate(self, request, mediaType, content):
		request.setHeader("content-type", mediaType)
		request.write(utils.bytify(content))
		request.finish()
		return server.NOT_DONE_YET

	def _getChildReal(self, name, request):
		raise svcs.UnknownURI("{self.__class__.__name__} has no children")

	def getChild(self, name, request):
		try:
			return self._getChildReal(utils.debytify(name), request)
		except Exception as exc:
			return dali.DALIErrorResource(exc)


class PersistentUploadManager(ResourceWithDALIErrors):
	def __init__(self, tableName):
		self.tableName = tableName
		resource.Resource.__init__(self)

	def render_POST(self, request):
		raise svcs.BadMethod("POST")
	
	def render_PUT(self, request):
		user = request.getAuthUser() or "anonymous"
		# for now, we try to get away with a computed physical name:
		# _<user>_<user-table-name>.  This *could* work because we
		# forbid leading underscores in our <user-table-name>-s.  This
		# will break (without delimited table identifiers) when our user
		# names become complicated.
		mediaType = request.getHeader("content-type")
		if mediaType is None:
			raise svcs.BadRequest("Missing payload media type")

		# normalise the media type so we can more easily recognised VOTables
		try:
			incomingFormat = formats.getMIMEFor(formats.getKeyFor(mediaType))
			if not "votable" in incomingFormat:
				raise formats.CannotSerializeIn("Really, parse from non-VOTable")
		except formats.CannotSerializeIn:
			raise svcs.BadRequest(f"Do not know how to ingest {mediaType} tables",
					hint="We accept VOTables with many media types; if in"
						" doubt, use application/x-votable+xml")

		# defer the rest into a thread; it's hard to make this async with
		# what we have.
		d = threads.deferToThread(self._perform_upload, request, user)
		d.addErrback(self._renderError, request)
		return server.NOT_DONE_YET

	def _perform_upload(self, request, user):
		with base.getWritableAdminConn() as conn:
			uploadUserTable(conn, self.tableName, request.content, user)
			
		self.serveImmediate(request, "text/plain",
			f"Query this table as tap_user.{self.tableName}\n")

	def render_GET(self, request):
		user = request.getAuthUser() or "anonymous"
		with base.getTableConn() as conn:
			try:
				rawTD = next(conn.query("SELECT metadata FROM tap_user.tables"
					" WHERE user_name=%(user)s and table_name=%(tablename)s",
					{"user": user, "tablename": self.tableName}))[0]
			except StopIteration:
				raise svcs.UnknownURI(f"No such uploaded table for user {user}")
		
		td = base.parseFromString(rscdef.TableDef, rawTD)
		td.parent = base.caches.getRD("//tap_user")

		et = tableset.getTableForTableDef(td, set(), rootElement=VTM.table)

		request.setHeader("content-type", "text/xml")
		utils.xmlwrite(et, request,
			prolog="<?xml-stylesheet href='/static/xsl/vosi.xsl' type='text/xsl'?>")
		return b""

	def render_DELETE(self, request):
		user = request.getAuthUser() or "anonymous"
		with base.getWritableAdminConn() as conn:
			try:
				# dropUserUploadedTable fails silently, but we want
				# to raise an error, so let's make sure the table
				# to drop actually exists.
				next(conn.query(
					"SELECT physical_name FROM tap_user.tables"
					" WHERE user_name=%(user)s and table_name=%(tablename)s",
					{"user": user, "tablename": self.tableName}))[0]
			except StopIteration:
				raise svcs.UnknownURI(f"No such uploaded table for user {user}")

			dropUserUploadedTable(conn, user, self.tableName)

		return self.serveImmediate(request,
			"text/plain", f"Dropped user table {self.tableName}\n")


class PersistentUploadRenderer(vodal.UnifiedDALRenderer):
	"""A renderer for creating and deleting persistent TAP-queriable tables.

	This really only makes sense on the TAP service.
	"""
	name = "user_tables"
	standardId = "ivo://ivoa.net/std/tap#persistent-uploads-1.2"
	resultType = "text/html"

	def render(self, request):
		user = request.getAuthUser() or "anonymous"
		if user=="anonymous":
			return dali.serveDALIError(
				request,
				svcs.ForbiddenURI("No persistent tables listing for"
					" anonymous users."))

		# this returns a VOSI tableset for the uploads made by this user
		with base.getTableConn() as conn:
			rawTDs = conn.query("SELECT metadata FROM tap_user.tables"
					" WHERE user_name=%(user)s", {"user": user})

		rd = base.caches.getRD("//tap_user")
		tds = []
		for (rawTD,) in rawTDs:
			td = base.parseFromString(rscdef.TableDef, rawTD)
			td.parent = rd
			tds.append(td)

		et = tableset.getTablesetForSchemaCollection(
			[(rd, tds)], rootElement=VTM.tableset)
		request.setHeader("content-type", "text/xml")
		utils.xmlwrite(et, request,
			prolog="<?xml-stylesheet href='/static/xsl/vosi.xsl' type='text/xsl'?>")
		return b""

	def getChild(self, name, request):
		try:
			name = name.decode("ascii", "replace")
			if not USER_TABLE_NAME_PATTERN.match(name):
				raise svcs.UnknownURI("You must give a valid upload table name here")

			with base.getTableConn() as conn:
				if base.UnmanagedQuerier(conn).getTableType("tap_user.tables"
						)!="BASE TABLE":
					raise svcs.ForbiddenURI(
						"Persistent table uploads not enabled on this instance.")

			return PersistentUploadManager(name)
		except Exception as exc:
			return dali.DALIErrorResource(exc)
