#!/usr/bin/python3

#c Copyright 2008-2023, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


import os
import sys

from setuptools import setup, find_packages, Extension

VERSION = "2.11"

if sys.hexversion<0x03090000:
	sys.exit("DaCHS2 definitely requires python 3.9 or later.")

if "DACHS_PIP_MESS" in os.environ:
	# So... a pip install . into a virtual python might very well work now.
	# Note that this is not tested regularly.  So, if there is breakage,
	# please report.
	install_requires = [
		"astropy", "cryptography", "docutils", "lxml", "matplotlib", "numpy", "Pillow", "setuptools", "psycopg2", "pymoc", "pyparsing", "rjsmin", "testresources", "twisted", "python-multipart", "PyOpenSSL",
	]
else:
	install_requires = []


SETUP_ARGS = {
	"name": "gavodachs",
	"description": "ZAH GAVO data center complete package",
	"url": "http://vo.ari.uni-heidelberg.de/soft",
	"license": "GPL",
	"author": "Markus Demleitner",
	"author_email": "gavo@ari.uni-heidelberg.de",
	"packages": find_packages(),
	# We're not zip-safe because the XSD validator accesses dist files by path
	"zip_safe": False,
	"include_package_data":  True,
	"install_requires": install_requires,
	"entry_points": {
		'console_scripts': [
			'gavo = gavo.user.cli:main',
			'dachs = gavo.user.cli:main',
		]
	},
	"version": VERSION,
}

if __name__=="__main__":
	setup(**SETUP_ARGS)
