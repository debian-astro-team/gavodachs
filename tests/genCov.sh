#!/bin/sh
# Determine coverage attained by running both unit and regression tests.
# Yeah, it'd be cooler if we'd get there using only unit tests; but
# that'd be a serious amount of work.

COVERAGE=python3-coverage

# forked.cov is filled by testhelpers.ForkingSubprocess, but that
# only appends; hence, we have to clean it up before running stuff.
rm -f forked.cov

export COVERAGE_FILE=unittests.cov
$COVERAGE run --source gavo runAllTests.py

export COVERAGE_FILE=regressiontests.cov
$COVERAGE run --source gavo -m gavo.user.cli serve debug > /dev/null 2>&1 &
srvpid=$!
export COVERAGE_FILE=testrunner.cov
$COVERAGE run --source gavo -m gavo.user.cli test -T120 ALL
kill $srvpid

export COVERAGE_FILE=../tests/docgen.cov
(cd ../docs; $COVERAGE run --source gavo -m gavo.user.cli gendoc refdoc > /dev/null)

export -n COVERAGE_FILE
$COVERAGE combine unittests.cov regressiontests.cov testrunner.cov\
	forked.cov trial.cov docgen.cov

$COVERAGE report
