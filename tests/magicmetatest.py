"""
Tests for special behaviour of meta values defined in base.typedmeta.
"""

from gavo.helpers import testhelpers

from gavo import base
from gavo.base import meta
from gavo.base import typedmeta
from gavo.web import metarender

from metatest import parseMetaXML, getMetaHTMLFor


class BibTeXTest(testhelpers.VerboseTest):
	def testBasic(self):
		mc = parseMetaXML("<meta>creator.name: Thor, A.U.\n"
			"creator:\ncreator.name: Bohr, N.\ntitle:On GAVO's role"
			" in Writing DaCHS &amp; \\\\expandafter\ncreationDate: 1984-08-05\n"
			"referenceURL:http://purl.org/pub/thor+bohr\n"
			"shortName:testres\n"
			"doi:10.542/testing</meta>")
		self.assertEqual(metarender.makeBibTeXForMetaCarrier(mc),
			r"""@MISC{vo:testres,
  year=1984,
  title={On {GAVO}'s role in Writing {DaCHS} \& \\expandafter},
  author={Thor, A.U. and Bohr, N.},
  url={http://purl.org/pub/thor+bohr},
  howpublished={{VO} resource provided by the Unittest Suite},
  doi = {10.542/testing}
}
""")

	def testIncompleteMetaErrors(self):
		mc = parseMetaXML("<meta>creator.name: Thor, A.U.</meta>")
		self.assertRaisesWithMsg(base.MetaError,
			"On <test meta carrier>: No meta item creationDate",
			metarender.makeBibTeXForMetaCarrier,
			(mc,))
	
	def testIdFallback1(self):
		mc = parseMetaXML("<meta>creator.name: Thor, A.U.\n"
			"creationDate: 1984-08-05T13:45:23\ntitle: Ulysses\n"
			"referenceURL:http://purl.org/pub/thor\n</meta>")
		self.assertTrue(metarender.makeBibTeXForMetaCarrier(mc
			).startswith("@MISC{vo:eiabtliumabb,"))

	def testTableNameMangling(self):
		mc = parseMetaXML("<meta>creator.name: Thor, A.U.\n"
			"creationDate: 1984-08-05T13:45:23\ntitle: ToC\n"
			"referenceURL:http://purl.org/pub/thor\n</meta>")
		mc.getQName = lambda: "mySchema.someTable"
		self.assertTrue(metarender.makeBibTeXForMetaCarrier(mc
			).startswith("@MISC{vo:mySchema_someTable,"))

	def testNoAuthor(self):
		mc = parseMetaXML("<meta>creationDate: 1984-08-05T13:45:23\n"
			"title: Der hessische Landbote\n"
			"referenceURL:http://purl.org/pub/thor\n</meta>")
		# The fallback to the DC creator is probably a bad idea here.
		# Well, any other behaviour is going to be still more painful.
		self.assertTrue(
			"author={Could be same as contact.name},"
			in metarender.makeBibTeXForMetaCarrier(mc))


class RightsTest(testhelpers.VerboseTest):
	def testCopyrightMaps(self):
		self.assertEqual(
			getMetaHTMLFor(
				'<meta name="copyright">Free to use</meta>',
				"rights"),
			'<span class="plainmeta">Free to use</span>')

	def testCombiningSynonyms(self):
		self.assertEqual(
			getMetaHTMLFor(
				'<meta name="_copyright">Do not dare touch this</meta>'
				'<meta name="copyright">Free to use</meta>'
				'<meta name="rights" format="rst">http://xkcd.com/2228/</meta>',
				"rights"),
			'<ul class="metaEnum"><li class="metaItem"><span class="plainmeta">'
			'Do not dare touch this</span></li>'
			'<li class="metaItem"><span class="plainmeta">Free to use</span></li>'
			'<li class="metaItem"><p><a class="reference external"'
			' href="http://xkcd.com/2228/">http://xkcd.com/2228/</a></p>'
			'\n</li></ul>')

	def testRenamingHappens(self):
		self.assertRaisesWithMsg(
			base.NoMetaKey,
			"On <test meta carrier>: No meta item copyright",
			getMetaHTMLFor,
			('<meta name="_copyright">Do not dare touch this</meta>', "copyright"))


class LinkTest(testhelpers.VerboseTest):
	def testLink(self):
		"""tests for working recognition of link-typed metas.
		"""
		mc = parseMetaXML('<meta name="_related" title="a link">'
			'http://foo.bar</meta>')
		self.assertEqual(mc.getMeta("_related").getContent("html"),
			'<a href="http://foo.bar">a link</a>')


class NoteTest(testhelpers.VerboseTest):
	def testCustomMetaIndentation(self):
		mc = parseMetaXML('\t\t<meta name="note" tag="6">\n'
			'\t\t\tA zero for the number of used images indicates that all images\n'
			'\t\t\thave some "problem" (such as overexposure).\n'
			'\t\t</meta>')
		self.assertEqual(mc.getMeta("note").children[0].content,
			'\nA zero for the number of used images indicates that all images\n'
			'have some "problem" (such as overexposure).')


class RelationshipTest(testhelpers.VerboseTest):
	def testRDAttribute(self):
		mc = parseMetaXML(
			'<meta name="isServedBy" ivoId="ivo://dc.g-vo.org/tap">'
			'GAVO TAP Service</meta>')
		self.assertEqual(
			str(mc.getMeta("isServedBy.ivoId")),
			"ivo://dc.g-vo.org/tap")
		self.assertEqual(
			str(mc.getMeta("isServedBy")),
			'GAVO TAP Service')
	
	def testRelationshipNames(self):
		self.assertEqual(
			list(meta.getKeysForClass(typedmeta.RelatedResourceMeta)), [
				'servedBy', 'serviceFor', 'relatedTo', 'mirrorOf', 'derivedFrom',
				'uses', 'cites', 'isSupplementTo', 'isSupplementedBy', 'isContinuedBy',
				'continues', 'isNewVersionOf', 'isPreviousVersionOf', 'isPartOf',
				'hasPart', 'isSourceOf', 'isDerivedFrom', 'isIdenticalTo',
				'isServiceFor', 'isServedBy'])


if __name__=="__main__":
	testhelpers.main(RelationshipTest)
