"""
Runs all unit tests defined for DaCHS.

This script *asssumes* it is run from tests subdirectory of the code
tree and silently won't work (properly) otherwise.

If ran with no arguments, it executes the tests from the current directory
and then tries to locate further, data-specific unit test suites.

If ran with the single argument "data", the program will read
$GAVO_INPUTS/__tests/__unitpaths__, interpret each line as a
inputs-relative directory name and run out-of-tree unittests there.

Location of unit tests: pyunit-based test suites are files matching
*test.py, trial-based suites are found by looking for test_*.py.
"""

#c Copyright 2008-2024, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


import os
import sys

if len(sys.argv)==1:
	pass
elif len(sys.argv)==2 and sys.argv[1]=="data":
	os.environ["GAVO_OOTTEST"] = "dontcare"
else:
	raise sys.exit(
		'%s takes zero arguments or just "data"'%sys.argv[0])

os.environ["GAVO_LOG"] = "no"

import unittest
import doctest
import glob
import subprocess


from gavo.helpers import testhelpers

import testresources

from gavo import base


def hasDoctest(fName):
	f = open(fName, "rb")
	tx = f.read()
	f.close()
	return b"doctest.testmod" in tx


def getDoctests():
	doctests = []
	for dir, dirs, names in os.walk("../gavo"):
		parts = dir.split("/")[1:]
		for name in [n for n in names if n.endswith(".py")]:
			try:
				if hasDoctest(os.path.join(dir, name)):
					name = ".".join(parts+[name[:-3]])
					doctests.append(doctest.DocTestSuite(name))
			except Exception:
				sys.stderr.write("*** While collecting doctests from %s:\n\n"%
					os.path.join(dir, name))
				raise
	return unittest.TestSuite(doctests)


def runTrialTests():
	"""runs trial-based tests, suppressing output, but raising an error if
	any of the tests failed.
	"""
	try:
		del os.environ["GAVO_INPUTSDIR"]
	except KeyError:
		pass
	trialTests = glob.glob("test_*.py")
	if trialTests:
		print("\nTrial-based tests:")

		args = ["-m", "twisted.trial", "--reporter", "text"
			]+[n[:-3] for n in trialTests]

		if "COVERAGE_FILE" in os.environ:
			os.environ["COVERAGE_FILE"] = "trial.cov"
			executor = ["python3-coverage", "run", "--source", "gavo"]
		# TODO: I think there's a pyannotate command line thingy that
		# we could run here as in python3-coverage.  However, merging
		# the two annotations doesn't look trivial, so I'm skipping this
		# for now.
		else:
			executor = ["python3"]

		subprocess.call(executor+args)


def runAllTests(includeDoctests=True):
	testhelpers.ensureResources()
	pyunitSuite = testresources.TestLoader().loadTestsFromNames(
		[n[:-3] for n in glob.glob("*test.py")])
	runner = unittest.TextTestRunner(
		verbosity=int(os.environ.get("TEST_VERBOSITY", 1)))
	if includeDoctests:
		pyunitSuite = unittest.TestSuite([pyunitSuite, getDoctests()])
	runner.run(pyunitSuite)
	runTrialTests()


def runDataTests():
	"""reads directory names from __tests/__unitpaths__ and then runs
	tests defined there.
	"""
	inputsDir = base.getConfig("inputsDir")
	dirFile = os.path.join(inputsDir, "__tests", "__unitpaths__")
	if not os.path.exists(dirFile):
		return
	with open(dirFile) as f:
		for dirName in f:
			dirName = dirName.strip()
			if dirName and not dirName.startswith("#"):
				os.chdir(os.path.join(inputsDir, dirName))
				curDir = os.getcwd()
				sys.path[0:0] = [curDir]

				print("\n\nTests from %s:\n\n"%dirName)
				runAllTests(includeDoctests=False)
				sys.path.remove(curDir)


def typesFilenameFilter(filename):
	"""normalises filename for pyannotate.

	That's None if the things shouldn't filter in the first place, a
	package-relative path.

	(actually, we're just looking for the last "gavo" in the path, but that
	should usually work well enough).
	"""
	if filename not in typesFilenameFilter.knownFiles:
		normalised = []
		for part in reversed(filename.split("/")):
			if part=="gavo":
				typesPath = "/".join(reversed(normalised))
				break
			normalised.append(part)
		else:
			typesPath = None

		typesFilenameFilter.knownFiles[filename] = typesPath

	return typesFilenameFilter.knownFiles[filename]

typesFilenameFilter.knownFiles = {}


if __name__=="__main__":
	base.DEBUG = False

	if len(sys.argv)==1:
		TYPE_STATS = os.environ.get("TYPE_STATS")
		if TYPE_STATS:
			from pyannotate_runtime import collect_types
			collect_types.init_types_collection(typesFilenameFilter)
			collect_types.collect()
			collect_types.start()

		try:
			runAllTests()
		finally:
			if TYPE_STATS:
				collect_types.stop()
				collect_types.dump_stats(TYPE_STATS)

		# Restart to let data tests run in non-testing environment.
		# We have to do that because data test don't sit in the test
		# inputs, and we can't really put them there.  Of course, that
		# lets unit tests ruin production data if worse comes to worst.
		subprocess.check_call(["python3", "runAllTests.py", "data"],
			env=testhelpers.originalEnvironment)

	elif sys.argv[1]=="data":
		runDataTests()

	else:
		sys.exit(f"Can't recognise arguments: {sys.argv}")
