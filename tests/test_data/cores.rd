<resource schema="test" resdir="" readProfiles="trustedquery,untrustedquery">
	<meta name="description">Helpers for tests for cores.</meta>
	<meta name="creationDate">2010-12-01T14:00:00</meta>
	<meta name="subject">testing</meta>
	<meta name="title">Test Fixtures</meta>
	<meta name="shortName">test ignore</meta>
	<meta name="doi">10.5072/test-fixture</meta>
	<FEED source="//procs#license-cc0" what="nothing in particular"/>
	<meta name="version">2.3.1pl2+git_deadbeef</meta>
	<meta name="creator">Gandalf, W.; Baggins, B.; Baggins, F.</meta>

	<meta name="schema-rank">20</meta>

	<meta name="isDerivedFrom" ivoId="ivo://x-testing/data/cores/abcd"
		>Some Table</meta>
	<meta name="isDerivedFrom" ivoId="ivo://x-testing/data/cores/basiccat"
		>Some Service</meta>
	<meta name="isIdenticalTo" ivoId="ivo://org.gavo.dc/cores/basiccat"
		>A mirror</meta>
	<meta name="_related" title="DataCite be damned"
		>http://just.any.old.url</meta>
	<meta name="_related" title="internallink"
		>\internallink{foo/bar/baz}</meta>

	<table id="abcd">
		<column name="a" type="text" verbLevel="1"/>
		<column name="b" type="integer" verbLevel="5">
			<values nullLiteral="-1"/>
		</column>
		<column name="c" type="integer" verbLevel="15">
			<values nullLiteral="-1"/>
		</column>
		<column name="d" type="integer" verbLevel="20" unit="km">
			<values nullLiteral="-1"/>
		</column>
		<column name="e" type="timestamp" verbLevel="25"/>
	</table>

	<data id="parse_text_table" auto="false">
		<reGrammar recordSep="&#10;" fieldSep="\s+">
			<names>a,b,c,d,e</names>
		</reGrammar>
		<make table="abcd">
			<rowmaker idmaps="a,b,c,d,e" id="parseProcessedRows"/>
		</make>
	</data>

	<pythonCore id="abccatcore">
		<inputTable>
			<LOOP listItems="a b c d e"><events>
				<inputKey original="abcd.\item"/>
			</events></LOOP>
		</inputTable>
		<outputTable original="abcd"/>
		<coreProc>
			<setup>
				<code>
					import tempfile
					from gavo import rsc, formats
				</code>
			</setup>
			<code>
				sourceTable = rsc.TableForDef(
					inputTable.inputTD.rd.getById("abcd"),
					rows=[inputTable.args])
				with tempfile.NamedTemporaryFile(mode="wb+") as f:
					formats.formatData("tsv", sourceTable, f)
					f.seek(0)
					return rsc.makeData(
						inputTable.inputTD.rd.getById("parse_text_table"),
						forceSource=f.name).getPrimaryTable()
			</code>
		</coreProc>
	</pythonCore>

	<service id="basiccat" core="abccatcore">
		<meta>
			moreExamples: convcat
			moreExamples.title: More from conv
			moreExamples: data/ssatest#s
			# fall through to default title
			moreExamples:http://ivoa.net/doc/obscore/tap-examples.xhtml
			moreExamples.title:More external
		</meta>

		<outputTable namePath="abcd">
			<outputField original="a"/>
		</outputTable>
	</service>

	<service id="convcat" core="abccatcore" allowed="form, static">
		<outputTable namePath="abcd">
			<column original="a" verbLevel="15"/>
			<column original="b" displayHint="sf=2"/>
			<column original="d" unit="m"/>
		</outputTable>
	</service>

	<service id="enums">
		<dbCore queriedTable="abcd">
			<condDesc>
				<inputKey original="a" required="False">
					<values fromdb="tableName from dc.tablemeta" multiOk="True"/>
				</inputKey>
			</condDesc>
			<condDesc>
				<inputKey original="b">
					<values><option>1</option><option>2</option></values>
				</inputKey>
			</condDesc>
			<condDesc required="True">
				<inputKey original="c">
					<values default="1"><option>1</option><option>2</option></values>
				</inputKey>
			</condDesc>
		</dbCore>
	</service>

	<service id="cstest" allowed="form, scs.xml">
		<meta name="shortName">really ignore</meta>
		<dbCore id="cscore" queriedTable="data/test#csdata">
			<FEED source="//scs#coreDescs"/>
			<condDesc buildFrom="mag"/>
			<condDesc>
				<inputKey original="rV" type="vexpr-float"
					>-100 .. 100<property name="cssClass">rvkey</property>
				</inputKey>
			</condDesc>
		</dbCore>
		<property name="customCSS">
			.rvkey { background:red; }
		</property>
		<publish render="form" sets="local"/>
	</service>

	<service id="grouptest" allowed="form,static">
		<dbCore queriedTable="data/test#adql">
			<condDesc>
				<inputKey original="rV"/>
			</condDesc>
			<inputTable id="knok">
				<group name="magic"
					description="Some magic parameters we took out of thin air.">
					<property name="cssClass">localstuff</property>
					<paramRef dest="mag"/>
					<paramRef dest="rV"/>
				</group>
				<inputKey original="rV">
					<values default="-4"/>
				</inputKey>
				<inputKey original="mag"/>
				<inputKey original="alpha"/>
				<inputKey original="delta"/>
			</inputTable>
		</dbCore>
	</service>

	<service id="impgrouptest" defaultRenderer="form">
		<dbCore queriedTable="data/test#adql">
			<condDesc>
				<inputKey original="rV">
					<property name="defaultForForm">-30</property>
				</inputKey>
				<inputKey original="mag"/>
				<group name="phys" description="Ah, you know.  The group with
					 the magic and the thin air">
					 <property name="label">Wonz</property>
					 <property name="style">compact</property>
				</group>
			</condDesc>
		</dbCore>
	</service>

	<service id="dl" allowed="dlget,dlmeta,dlasync">
		<meta name="title">Hollow Datalink</meta>
		<meta name="shortName">Hollow DL</meta>
		<publish render="dlmeta" sets="ivo_managed"/>
		<datalinkCore>
			<descriptorGenerator procDef="//soda#fits_genDesc">
				<setup>
					<code>
						from gavo import svcs
						from gavo.protocols import soda
					</code>
				</setup>
				<code>
					if pubDID=="broken":
						ddt
					elif pubDID=="somewhereelse":
						raise svcs.WebRedirect("http://some.whereel.se/there")
					return getFITSDescriptor(pubDID)
				</code>
			</descriptorGenerator>
			<metaMaker procDef="//soda#fits_makeWCSParams" name="getWCSParams">
				<bind key="axisMetaOverrides">{3: {}}</bind>
			</metaMaker>
			<dataFunction procDef="//soda#fits_makeHDUList" name="makeHDUList"/>
			<FEED source="//soda#fits_Geometries"/>
			<FEED source="//soda#fits_standardBANDCutout" spectralAxis="3"
				wavelengthOverride="Angstrom"/>
			<dataFunction procDef="//soda#fits_doWCSCutout" name="doWCSCutout"/>
			<FEED source="//soda#fits_genPixelPar"/>
			<FEED source="//soda#fits_genKindPar"/>
			<dataFormatter procDef="//soda#fits_formatHDUs" name="formatHDUs"/>
		</datalinkCore>
		
		<meta name="_example" title="Example 1">
			This is an example for examples, describing
			:dl-id:`ivo://org.gavo.dc/~?bla/foo/qua` (which, incidentally,
			does not exist).
		</meta>

		<meta name="_example" title="Example 2">
			This is another example for examples.
		</meta>
	</service>

	<dbCore id="typescore" queriedTable="data/test#typesTable"/>

	<table id="conecat" onDisk="True" mixin="//scs#q3cindex">
		<stc>
			POSITION FK5 J1990 Epoch J1985.7 "ra" "dec"
		</stc>
		<column name="id" type="integer" ucd="meta.id;meta.main" required="True"/>
		<column name="ra" type="real" ucd="pos.eq.ra;meta.main"/>
		<column name="dec" type="double precision" ucd="pos.eq.dec;meta.main"/>
	</table>

	<data id="import_conecat">
		<sources item="nix"/>
		<embeddedGrammar>
			<iterator>
				<code>
					for id, (ra, dec) in enumerate([(1.25, 2.5), (23, -92.5)]):
						yield locals()
				</code>
			</iterator>
		</embeddedGrammar>
		<make table="conecat"/>
	</data>

	<service id="scs" allowed="scs.xml, api, form" defaultRenderer="scs.xml">
		<meta name="testQuery">
			<meta name="ra">10</meta>
			<meta name="dec">20</meta>
			<meta name="sr">1</meta>
		</meta>
		<meta name="shortName">test ignore</meta>
		<scsCore queriedTable="conecat">
    	<FEED source="//scs#coreDescs"/>
    	<condDesc buildFrom="id"/>
		</scsCore>
		<publish sets="local"/>

		<FEED source="//pql#DALIPars"/>

	</service>

	<coverage>
		<spatial>1/10</spatial>
		<temporal>54444 55555</temporal>
		<temporal>56083 56087</temporal>
		<spectral>0.3e-19 1e-18</spectral>
		<spectral>1e-17 5e-17</spectral>
	</coverage>

	<service id="uploadtest" allowed="api,form,siap.xml">
		<meta name="crash_this">This will crash availability for testing.</meta>
		<debugCore>
			<inputTable>
				<inputKey name="notarbitrary" type="file"
					description="An example upload containing nothing in particular"
					ucd="par.upload">
					<property name="adaptToRenderer">True</property>
				</inputKey>
			</inputTable>
		</debugCore>
		<inputKey name="frobnicate" type="boolean"
			description="A service key"/>
		<FEED source="//pql#DALIPars"/>
	</service>

	<service id="rds" allowed="static">
		<template key="fixed">defaultresponse.html</template>
		<nullCore/>
		<property name="staticData">data</property>
	</service>

	<service id="pc" allowed="api,form,uws.xml,async">
		<meta name="title">PC datalink</meta>
		<meta name="shortName">PC datalink</meta>
		<publish sets="ivo_managed" render="api"/>
		<pythonCore>
			<inputTable>
				<inputKey name="opre" description="Operand, real part"
					required="True"/>
				<inputKey name="opim" description="Operand, imaginary part">
					<values default="1.0"/>
				</inputKey>
				<inputKey name="powers" description="Powers to compute"
					type="integer[]" multiplicity="single">
					<values default="1 2 3"/>
				</inputKey>
				<inputKey name="responseformat" description="Preferred
						output format" type="text">
							<values default="application/x-votable+xml"/>
				</inputKey>
				<inputKey name="stuff" type="file" description="Stuff to upload"/>
			</inputTable>
			<outputTable>
				<outputField name="re" description="Result, real part"/>
				<outputField name="im" description="Result, imaginary part"/>
				<outputField name="log_value"
					description="real part of logarithm of result"/>
			</outputTable>

			<coreProc>
				<setup imports="cmath"/>
				<code>
					powers = inputTable.args["powers"]
					op = complex(inputTable.args["opre"],
						inputTable.args["opim"])
					rows = []
					for p in powers:
						val = op**p
						rows.append({
							"re": val.real,
							"im": val.imag,
							"log_value": cmath.log(val).real})
				
					if hasattr(inputTable, "job"):
						with inputTable.job.getWritable() as wjob:
							wjob.addResult("Hello World.\\n", "text/plain", "aux.txt")

					t = rsc.TableForDef(self.outputTable, rows=rows)
					t.addMeta("info", str(inputTable.args["opim"]),
						infoName="inpar",
						infoValue="opim")
					return t
				</code>
			</coreProc>
		</pythonCore>
	</service>

	<service id="cc" allowed="qp,static">
		<meta name="title">custom core tester</meta>
		<property name="indexFile">does-not-exist.testing</property>
		<property key="queryField">x</property>
		<customCore module="data/testcore"/>
	</service>

	<service id="uc" allowed="uws.xml,async">
		<pythonCore>
			<inputTable>
				<inputKey name="stuff" type="file" description="Stuff to upload"/>
				<inputKey name="other" type="file"
					description="More stuff to upload"/>
			</inputTable>
			<outputTable/>

			<coreProc>
				<code>
					return None
				</code>
			</coreProc>
		</pythonCore>
	</service>

	<table id="coltesttable">
		<column name="a" verbLevel="1"/>
		<column name="b" verbLevel="10"/>
		<column name="c" verbLevel="20" displayHint="noxml=true"/>
		<column name="d" verbLevel="30"/>
		<column name="e" verbLevel="30"/>
		<column name="f" verbLevel="40"/>
	</table>

	<service id="coltest0" allowed="api,form">
		<dbCore queriedTable="coltesttable"/>
	</service>

	<service id="coltest" original="coltest0"
			allowed="external,static, volatile,form">
		<template key="fixed">defaultresponse.html</template>
		<outputTable>
			<column original="a" sets="kl,gr"/>
			<column original="b" sets="gr"/>
			<column original="c" sets="kl"/>
			<column original="d" sets="ALL"/>
			<column original="f"/>
		</outputTable>
	</service>

	<service id="coltestr" original="coltest">
		<property name="votableRespectsOutputTable">True</property>
	</service>

	<service id="singlearg" allowed="api,qp,external">
		<meta name="shortName">sing</meta>
		<property name="queryField">mainarg</property>
		<publish render="external">
			<meta name="accessURL">http://supersmart.edu/singlearg</meta>
		</publish>

		<pythonCore>
			<inputTable>
				<inputKey name="mainarg" type="text"/>
				<inputKey name="otherarg" type="integer">
					<values default="3" nullLiteral="-1"/>
				</inputKey>
			</inputTable>
			<outputTable>
				<column name="ct" type="integer" description="I'm count"
					required="True"/>
				<column name="val" type="text" description="I'm not value"/>
			</outputTable>
			<coreProc>
				<code>
					item = inputTable.args["mainarg"]
					if item=="fail hard":
						ddt
					queryMeta["Matched"] = inputTable.args["otherarg"]
					return rsc.TableForDef(self.outputTable,
						rows=[{"ct": rowInd+1, "val": item*(rowInd+1)}
							for rowInd in range(inputTable.args["otherarg"])])
				</code>
			</coreProc>
		</pythonCore>
	</service>

	<service id="wfq" allowed="fixed">
		<template key="fixed">fixtest.html</template>
		<fixedQueryCore query="SELECT 2 as num1, 3 as num2 from generate_series(1, 3)">
			<outputTable>
				<outputField name="num1"/>
				<outputField name="num2"/>
			</outputTable>
		</fixedQueryCore>
	</service>

	<service id="maxrecdl" allowed="dlmeta">
		<datalinkCore>
			<descriptorGenerator>
				<code>
					return ProductDescriptor(pubDID, pubDID, None, "vnd/x-bogus")
				</code>
			</descriptorGenerator>
			<metaMaker>
				<code>
					for i in range(10):
						yield descriptor.makeLink("http://%s.example.org/"
							+descriptor.pubDID)
				</code>
			</metaMaker>
		</datalinkCore>
	</service>

	<service id="keepout" limitTo="testing" original="coltest"/>

	<service id="zombie">
		<meta name="superseded" format="rst">
			Brains!  Brains! Brains_!

			.. _Brains: https://en.wikipedia.org/wiki/George_A._Romero
		</meta>
		<nullCore/>
	</service>

	<procDef id="make-fits-desc" type="descriptorGenerator">
		<doc>
			A descriptor generator producing some FITS header structure.

			This has a few built-in defaults; the id is a urlencoded extra
			header cards.
		</doc>
		<setup imports="gavo.protocols.datalink">
			<code>
			def perhapsFloat(v):
				try:
					num = float(v)
					if num-int(num)==0:
						return int(num)
					else:
						return num
				except ValueError:
					return v

			class PD(datalink.DatalinkDescriptorMixin):
				def __init__(self, extraCards):
					self.pubDID = extraCards
					self.accref = f"testing {extraCards}"
					self.accessPath = f"Forget/it"
					self.mime = "application/fits"
					self.imageExtind = 0
					self.hdr = {
						"CRVAL1": 0,   "CRVAL2": 0,
						"CRPIX1": 50,  "CRPIX2": 50,
						"CD1_1": 0.01, "CD1_2": 0,
						"CD2_1": 0,    "CD2_2": 0.01,
						"NAXIS1": 100, "NAXIS2": 100,
						"CUNIT1": "deg", "CUNIT2": "deg",
						"CTYPE1": 'RA--TAN', "CTYPE2": 'DEC-TAN',
						"LONPOLE": 180.,
						"NAXIS": 2,
					}
					self.hdr.update(dict((k, perhapsFloat(v[0])) for k, v in
						urllib.parse.parse_qs(extraCards).items()))
					self.slices = []
					self.data = ["This is test instrumentation"]
				
				def changingAxis(self, axisIndex, parName):
					pass
			</code>
		</setup>
		<code>
			return PD(pubDID)
		</code>
	</procDef>

	<procDef id="debug-formatter" type="dataFormatter">
		<code>
			return "text/plain", " ".join(str(v) for v in descriptor.slices)
		</code>
	</procDef>
</resource>
