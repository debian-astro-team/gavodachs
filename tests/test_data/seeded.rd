<resource schema="test">
	<meta name="creationDate">2020-03-04T13:11:00</meta>
	<meta name="title">Injection testing</meta>
	<meta name="description">An RD to exercise from-DB metadata injection.</meta>
	<meta name="shortName">inj</meta>
	<meta name="subject">testing</meta>

	<table id="seedy" onDisk="True" adql="True">
		<publish/>
		<column name="h"/>
	</table>

	<coverage>
		<temporal>2000-01-01 2000-02-01</temporal>
		<temporal>54322.25 54333.75</temporal>
	</coverage>

	<data id="import">
		<sources item="10"/>
		<embeddedGrammar>
			<iterator>
				<code>
					for x in range(int(self.sourceToken)):
						yield {"h": x/2.}
				</code>
			</iterator>
		</embeddedGrammar>
		<make table="seedy"/>
	</data>

	<service id="hooray">
		<publish render="form" sets="local,ivo_managed"/>
		<meta name="shortName">inj hooray</meta>
		<dbCore queriedTable="seedy"/>
	</service>

	<service id="whoompa" allowed="external">
		<meta name="identifier">ivo://x-testing/manual/override/whoompa</meta>
		<meta name="shortName">inj whoompa</meta>
		<publish render="external" sets="local">
			<meta name="accessURL">http://invalid/whoompa</meta>
		</publish>
		<nullCore/>
	</service>
</resource>
