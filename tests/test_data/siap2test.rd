<resource schema="test" resdir="data">
	<meta name="creationDate">2023-03-15T12:45:01Z</meta>
	<meta name="title">DaCHS SIAP2 unittest resource</meta>
	<meta name="description">You should not see this.</meta>
	<meta name="subject">virtual-observatories</meta>
	<meta name="creator.name">Hendrix, J.; Page, J; et al.</meta>
	<meta name="creator.name">The Master Tester</meta>

	<table id="siap2" onDisk="True" mixin="//siap2#pgs" adql="True">
		<column name="extracolumn" type="integer" required="True"/>
	</table>

	<data id="import">
		<sources pattern="ex.fits"/>
		<fitsProdGrammar>
			<rowfilter procDef="//products#define">
				<bind key="table">"test.siap2"</bind>
			</rowfilter>
		</fitsProdGrammar>
		<make table="siap2" role="primary">
			<rowmaker>
				<apply procDef="//siap2#computePGS"/>
				<apply procDef="//siap2#setMeta">
					<bind key="obs_collection">"DaCHS regression"</bind>
					<bind key="obs_title">"Example Observation"</bind>
					<bind key="t_min">54253.2</bind>
					<bind key="t_max">54253.3</bind>
					<bind key="t_exptime">8640</bind>
					<bind key="bandpassId">'Ks'</bind>
					<bind key="facility_name">'DaCHS labs'</bind>
				</apply>
				<apply procDef="//siap2#getBandFromFilter"/>
				<map key="extracolumn">5</map>
			</rowmaker>
		</make>
	</data>

	<service id="svc" allowed="siap2.xml">
		<meta name="shortName">Ha-Tester</meta>
		<meta name="sia.type">Pointed</meta>
		<meta name="testQuery.pos.ra">163.3</meta>
		<meta name="testQuery.pos.dec">57.8</meta>
		<meta name="testQuery.size.ra">0.1</meta>
		<meta name="testQuery.size.dec">0.1</meta>

		<publish render="siap2.xml" sets="ivo_managed"/>
		<dbCore queriedTable="siap2">
			<FEED source="//siap2#parameters"/>
		</dbCore>
	</service>
</resource>
