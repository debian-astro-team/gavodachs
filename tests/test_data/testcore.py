from gavo import api

class Core(api.Core):
	inputTableXML = """
    <inputTable>
      <inputKey name="x" type="integer" multiplicity="single"
        tablehead="Num"/></inputTable>"""

	def run(self, service, inputTable, queryMeta):
		return ("text/plain", "x"*inputTable.args["x"])
