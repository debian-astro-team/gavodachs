<resource schema="__test">
	<meta name="title">GAVO Test Data</meta>
	<meta name="creationDate">2008-10-15T15:01:03</meta>
	<meta name="subject">Testing</meta>
	<meta name="creator">\metaString{publisher}</meta>
	<macDef name="testmacro">expanded!</macDef>
	<meta name="description">It's \testmacro</meta>

	<meta>
		instrument: Instrument without identity
		instrument: Instrument with ivoid
		instrument.ivoId: ivo://x-example/inst1
		instrument: Instrument with ivoid and doi
		instrument.ivoId: ivo://x-example/inst2
		instrument.altIdentifier: doi:107331/fakefake
	</meta>

	<table id="data">
		<column name="anint" tablehead="An Integer" type="integer">
			<values nullLiteral="-2147483648"/>
		</column>
		<column name="afloat" tablehead="Some Real"/>
		<column name="atext" type="text"
			tablehead="A string must be in here as well">
			<values><option>bla</option><option>blubb</option></values>
		</column>
		<column name="adate" tablehead="When" type="date" unit="d"/>
	</table>
	
	<table id="barsobal">
		<register services="//services#overview" sets="ignore"/>
		<column name="anint" type="integer">
			<values nullLiteral="-2147483648"/>
		</column>
		<column name="adouble" tablehead="And a Double"
			type="double precision"/>
	</table>

	<data id="twotables">
		<dictlistGrammar/>
		<make table="data" role="primary"/>
		<make table="barsobal"/>
	</data>

	<table id="nork">
		<column name="cho" type="text">
			<values><option>a</option><option>b</option></values>
		</column>
	</table>

	<data id="ronk">
		<make table="barsobal"/>
	</data>

	<table id="haslinks" onDisk="True">
		<column name="prod_id" type="text"/>
	</table>

	<data id="import-biblinks">
		<sources item="2"/>
		<embeddedGrammar><iterator><code>
			for rowid in range(int(self.sourceToken)):
				yield {"prod_id": "prod{}.bin".format(rowid)}
		</code></iterator></embeddedGrammar>
		<make table="haslinks">
			<script type="postCreation" lang="python" name="add biblinks">
				from gavo.protocols import biblinks

				biblinks.clearLinks(table.connection, table.tableDef.rd)
				src_id = base.getMetaText(
					table.tableDef.rd.getById("haslinks"), "identifier")
				biblinks.defineLinks(table.connection,
					table.tableDef.rd,
					[(pub, "IsSupplementedBy", src_id)
						for pub in ["2015A&amp;C....10...88D", "2005ASPC..347...29T"]])

				biblinks.defineLinks(table.connection,
					table.tableDef.rd,
					[("10.1051/0004-6361:20010923", "Cites", makeProductLink(
							rec["prod_id"]), 5)
						for rec in table],
					bibFormat="doi")
			</script>
		</make>
	</data>
</resource>
