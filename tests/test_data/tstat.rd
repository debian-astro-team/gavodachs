<resource schema="test">
	<meta name="description">A data descriptor for tests relating to
		obtaining column statistics.</meta>
	
	<table id="DoNotMixCase" onDisk="True">
		<column name="plain_float">
			<values min="-1"/>
		</column>
		<column name="quoted/gg-/l" type="bigint">
			<values nullLiteral="-1"/>
		</column>
		<column name="dontlook">
			<property name="statistics">no</property>
			<values min="-3.6"/>
		</column>
		<column name="allnull"/>
		<column name="hurgel" type="text">
			<property name="statistics">enumerate</property>
		</column>
	</table>

	<data id="import">
		<sources><item>5</item></sources>
		<embeddedGrammar>
			<iterator>
				<code>
					for i in range(1, int(self.sourceToken)):
						yield {
							"plain_float": 1./i,
							"tomunge": i**2,
							"dontlook": 4,
							"allnull": None,
							"hurgel": f"nma{i%2}",
						}
				</code>
			</iterator>
		</embeddedGrammar>
		<make table="DoNotMixCase">
			<rowmaker idmaps="*">
				<map key="quoted/gg-/l">@tomunge</map>
			</rowmaker>
		</make>
	</data>
</resource>
