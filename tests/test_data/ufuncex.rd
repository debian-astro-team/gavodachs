<resource schema="test" readProfiles="trustedquery,untrustedquery">
	<!-- a table to provide material for ufunctests. -->

	<table id="ufuncex" onDisk="True" adql="True" mixin="//scs#q3cindex">
		<column name="testgroup" type="text"/>
		<column name="dt" type="timestamp"/>
		<column name="ra" ucd="pos.eq.ra;meta.main" unit="deg"/>
		<column name="dec" ucd="pos.eq.dec;meta.main" unit="deg"/>
		<column name="p" type="spoint"/>
		<column name="m" type="smoc"/>
		<column name="freq" unit="GHz"/>
	</table>

	<data id="import">
		<sources items="0"/>
		<embeddedGrammar>
			<iterator><code>
				rec = {"testgroup": None, "dt": None, "freq": None}

				rec["testgroup"] = "jd"
				rec["dt"] = datetime.datetime(year=1984, month=8, day=5, hour=12)
				rec["ra"], rec["dec"] = 23.5, -12.25
				rec["p"] = pgsphere.SPoint.fromDegrees(230, 60)
				yield rec

				rec["testgroup"] = "intervals"
				rec["dt"] = None
				rec["p"] = pgsphere.SPoint.fromDegrees(10, 20)
				rec["ra"] = 10
				rec["dec"] = 20
				yield rec

				rec["p"] = pgsphere.SPoint.fromDegrees(50, 80)
				rec["ra"] = 50
				rec["dec"] = 80
				yield rec

				rec["ra"] = rec["dec"] = rec["p"] = None
				rec["testgroup"] = "moc"
				rec["m"] = pgsphere.SMoc.fromASCII("6/33 7/50-51")
				yield rec
				rec["m"] = pgsphere.SMoc.fromASCII("6/38-40 7/49-50")
				yield rec

				rec["testgroup"] = "specconv"
				rec["freq"] = 21
				yield rec
			</code></iterator>
		</embeddedGrammar>
		<make table="ufuncex"/>
	</data>

	<table id="arrsample" onDisk="True" adql="True">
		<column name="recid" type="integer" ucd="meta.id;meta.main"/>
		<column name="flux" type="real[]" ucd="phot.mag" unit="Jy"
			description="Some flux values in an array"/>
		<column name="plc" type="double precision[]" ucd="pos.cartesian"
			unit="m" description="Possibly a cartesian position"/>
	</table>

	<data id="import_arr">
		<sources><item>1</item><item>2</item></sources>
		<embeddedGrammar>
			<iterator>
				<setup imports="random"/>
				<code>
					id = int(self.sourceToken)
					random.seed(id)
					yield {
						"recid": id,
						"flux": [random.random() for _ in range(6)],
						"plc": [random.random()*4-2 for _ in range(3)],
						}
					# don't give the remaining tests a constant RNG
					random.seed()
				</code>
			</iterator>
		</embeddedGrammar>
		<make table="arrsample"/>
	</data>

</resource>
