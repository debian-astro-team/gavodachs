<resource schema="test" readProfiles="trustedquery,untrustedquery">
	<table id="testjobs">
		<FEED source="//uws#uwsfields"/>
		<column name="magic" type="text" description="extra magic attribute
			exclusive to testjobs"/>
	</table>
	
	<data id="import">
		<make table="testjobs"/>
	</data>

	<service id="borken" allowed="siap.xml">
		<meta name="description">A test without sufficient metadata so
			you can't produce a capability record for.

			(this is unrelated to uws, but we don't disturb other services here)
		</meta>
		<nullCore/>
		<publish render="siap.xml" sets="ivo_managed"/>
		<publish render="form" sets="ivo_managed"/>
	</service>
</resource>
