#!/bin/bash
# add annotated modules as you go
#
# I'm trying to speed this up using pyannotate, but regrettably
# so far it only finds functions in the test modules. Once this
# works, you can say export TYPE_STATS=type.stats, run a test
# and pick out the annotations found from type.stats.
annotated="
gavo.utils.algotricks
gavo.utils.autonode
gavo.utils.codetricks
gavo.utils.dachstypes
gavo.utils.excs
gavo.utils.fitstools
gavo.utils.imgtools
gavo.utils.mathtricks
gavo.utils.misctricks
gavo.utils.ostricks
gavo.utils.pgsphere
gavo.utils.plainxml
gavo.utils.serializers
gavo.utils.stanxml
gavo.utils.texttricks"

cd ..
mypy -m $(echo $annotated | sed -e 's/ / -m /g')
