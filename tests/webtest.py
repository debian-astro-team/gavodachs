"""
Tests for rendering web pages that do not require a running reactor
(the latter ones go to test_web.py, which is run by trial).
"""

#c Copyright 2008-2024, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


from gavo.helpers import testhelpers

from gavo import base
from gavo import formal
from gavo import rscdesc
from gavo.helpers import trialhelpers
from gavo.web import metarender

import tresc

def _renderPage(pageClass, path, *args, debug=False):
	request = trialhelpers.FakeRequest(path)
	page = pageClass(request, *args)
	return testhelpers.getXMLTree(
		formal.flattenSync(
			page._getDoc(request), request), debug=debug)


class _ResrecRD(testhelpers.TestResource):
	def make(self, deps):
		return base.parseFromString(rscdesc.RD,
			f"""<resource schema="test"><table id="foo" onDisk="True">
				<meta name="title">Foo-Bar</meta>
				<meta name="description">The foo data</meta>
				<column name="quux"/></table>
				{tresc.A_RES_REC}
				</resource>""")


class RDInfoTest(testhelpers.VerboseTest):
	resources = [("rrd", _ResrecRD())]

	def testServiceAndTables(self):
		tree = _renderPage(metarender.RDInfoPage, "browse/data/cores",
			base.caches.getRD("data/cores"), debug=False)

		self.assertEqual(tree.xpath("//h2[@id='rdi-svcs']")[0].text,
			"Services defined within this resource descriptor")
		self.assertEqual(tree.xpath("//h2[@id='rdi-tables']")[0].text,
			"Tables defined within this resource descriptor")

		self.assertEqual(
			tree.xpath("//div[@id='body']/ul[1]/li[1]/a")[0].get("href"),
			"http://localhost:8080/data/cores/dl/info")
		self.assertEqual(
			tree.xpath("//div[@id='body']/ul[1]/li[1]/a")[0].text,
			"Hollow Datalink")

		self.assertEqual(tree.xpath(
			"//div[@id='body']/ul[2]/li[1]/a")[0].get("href"),
			"/tableinfo/test.conecat")
		self.assertEqual(
			tree.xpath("//div[@id='body']/ul[2]/li[1]/a")[0].text,
			"test.conecat")

		self.assertEqual(len(tree.xpath("//h2[@id='rdi-other']")), 0)

	def testTableAndOther(self):
		tree = _renderPage(metarender.RDInfoPage, "browse/anything/foo",
			self.rrd, debug=False)
		self.assertEqual(len(tree.xpath("//h2[@id='rdi-svcs']")), 0)
		self.assertEqual(
			tree.xpath("//div[@id='body']/ul[1]/li[1]/a")[0].get("href"),
			"/tableinfo/test.foo")
		self.assertEqual(
			tree.xpath("//div[@id='body']/ul[1]/li[1]//p")[0].text,
			"The foo data")
		self.assertEqual(
			tree.xpath("//div[@id='body']/ul[2]/li[1]/a")[0].get("href"),
			"http://localhost:8080/temporary/rec/info")

		self.assertEqual(tree.xpath("//h2[@id='rdi-other']")[0].text,
			"Other Resources defined here")

	def testResrecInfo(self):
		tree = _renderPage(metarender.ServiceInfoRenderer, "browse/anything/foo",
			self.rrd.getById("rec"), debug=False)
		self.assertEqual(tree.xpath("//div[h4='Identifier']//span")[0].text,
			"ivo://foo.bar")
		self.assertEqual(len(tree.xpath("//em[.='edition']")), 6)
		self.assertEqual(
			tree.xpath("//li[@class='capability'][1]/a")[0].get("href"),
			"http://localhost:8080/temporary/rec/edition")
	
	def testEditionRender(self):
		tree = _renderPage(metarender.EditionRenderer, "browse/anything/foo",
			self.rrd.getById("rec"), debug=False)
		body = tree.xpath("//div[@id='body']")[0]
		self.assertEqual(body.xpath("ul/li[1]/a/@href")[0],
			"http://foo.us/fancy/doc")
		self.assertEqual(
			body.xpath("ul/li[1]/span[@class='plainmeta']")[0].text,
			"svn://foo.us/docs/src/fancy")
		self.assertEqual(
			body.xpath("ul/li[2]/em[@class='langcode']/span")[0].text,
			"fr")
		self.assertEqual(
			body.xpath("ul/li[3]/em/span[@class='plainmeta']")[0].text,
			"Ja, geh zu.")


class _RenderedServiceInfo(testhelpers.TestResource):
	def make(self, deps):
		return _renderPage(metarender.ServiceInfoRenderer, "/data/cores/pc/info",
				base.caches.getRD("data/cores").getById("pc"), debug=False)


class ServiceInfoTest(testhelpers.VerboseTest):
	resources = [("tree", _RenderedServiceInfo())]

	def testSpatialCoverage(self):
		self.assertEqual(
			testhelpers.pickSingle(
				self.tree.xpath("//img[@class='svc-coverage']/@src")),
			"coverage")
	
	def testSpectralCoverage(self):
		covEl = testhelpers.pickSingle(
			self.tree.xpath("//p[@id='spectral-coverage']"))
		self.assertEqual(covEl.text, '0.187245 6.24151 eV')
		self.assertEqual(len(covEl), 2)
		self.assertEqual(covEl[0].tail, '62.4151 312.075 eV')

	def testTemporalCoverage(self):
		covEl = testhelpers.pickSingle(
			self.tree.xpath("//p[@id='temporal-coverage']"))
		self.assertEqual(covEl.text, '2007.94 2010.98')
		self.assertEqual(len(covEl), 2)
		self.assertEqual(covEl[0].tail, '2012.43 2012.44')


if __name__=="__main__":
	testhelpers.main(RDInfoTest)

